package com.company;
import java.util.*;
import java.io.*;
public class WithdrawManagement {
    public static void validateAuth(String accountId,String password)throws Exception{
        int incrementIfFound = 0;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        String records;
        while((records=reader.readLine())!= null ){
            if(records.contains(accountId) && (records.contains(password))) {
                incrementIfFound++;
            }
        }
        if(incrementIfFound ==  1 ){
        }
        else if(incrementIfFound == 0){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |             Warnings : Please account ID or password are incorrect                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            withdrawMoneyOptions();
        }
        else{
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                      Error : Oops , System is down                                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            withdrawMoneyOptions();
        }
    }

    public static void withdrawMoney(String AccountId,String Password,Double money) throws Exception {
        String accountId = null;
        String fullname;
        String ministry;
        String balance;
        String initBalance;
        String password;
        Double BalanceIn;
        String type;
        int noAccount = 0;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));
        File tempFile = new File("temp.txt");
        tempFile.createNewFile();
        BufferedWriter  writer = new BufferedWriter(new FileWriter(tempFile));
        String records;

        while ((records = reader.readLine()) != null){
            StringTokenizer st = new StringTokenizer(records,",");
            if((records.contains(AccountId)) && records.contains(Password)){
                noAccount ++;
                accountId = st.nextToken();
                fullname = st.nextToken();
                ministry = st.nextToken();
                initBalance = st.nextToken();
                balance = st.nextToken();
                password = st.nextToken();
                type = st.nextToken();

                BalanceIn = Double.parseDouble(balance);

                if( (money < BalanceIn) || (money.equals(BalanceIn)) ){
                    BalanceIn = BalanceIn - money;
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                        System.out.println("\t\t\t\t\t\t\t |             Success :       You have withdrawn "+money+" frw from your account                       |");
                        System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                }
                else{
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                        System.out.println("\t\t\t\t\t\t\t |             Warnings :       You want to withdraw money greater to your balance                   |");
                        System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    withdrawMoneyOptions();
                }
                writer.write(accountId+","+fullname+","+ministry+","+initBalance+","+BalanceIn+","+password+","+type);
                writer.flush();
                writer.newLine();
            }
            else{
                writer.write(st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken());
                writer.flush();
                writer.newLine();
            }
        }
        writer.close();
        accountFile.delete();
        tempFile.renameTo(accountFile);
        
        
        /*
                    SAVING THE TRANSACTION
         */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer st = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(st.nextToken());
        }
        Transaction transaction = new Transaction(accountId,null,0,money,"Withdrawing is done on account",agentId);
        transaction.saveTransaction();
    }
    public static void withdrawMoneyOptions() throws Exception {
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        Double moneyToWithDraw;
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO WITHDRAW FROM  ACCOUNT            |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId,password);

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|               LOGIN COMPLETED --  NOW YOU CAN WITHDRAW                   |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER MONEY TO WITHDRAW           :\t\t");
        moneyToWithDraw = scan.nextDouble();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        withdrawMoney(accountId,password,moneyToWithDraw);
        BankSystemOptions.withdrawManagement();
    }
}
