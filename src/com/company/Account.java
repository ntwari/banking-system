package com.company;
import java.io.*;
import java.util.Date;
import java.util.Objects;
import java.util.Random;
import java.util.StringTokenizer;

public class Account {
    private int account_id;
    private String password;
    private String user_name;
    private Double initial_balance;
    private Double balance;
    private String ministry;
    private Date createdAt;

    public Account( String password, String user_name, Double initial_balance, String ministry) {
        this.password = password;
        this.user_name = user_name;
        this.initial_balance = initial_balance;
        this.balance = this.initial_balance;
        this.ministry = ministry;
    }

    // Account id made here by hashing username , password and random number

    public void setAccount_id(){
//        Random randomlyNumber = new Random(10);
//        int randomlyNumber = 400;
        this.account_id = Objects.hash(this.user_name,this.password);
    }

    public int getAccount_id() {
        return this.account_id;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_name() {
        return this.user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Double getInitial_balance() {
        return this.initial_balance;
    }

    public void setInitial_balance(Double initial_balance) {
        this.initial_balance = initial_balance;
    }

    public Double getBalance() {
        return this.balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getMinistry() {
        return this.ministry;
    }

    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

    public void saveAccount()throws Exception{
        /*
                HASHING THE ACCOUNT ID
         */
        setAccount_id();

        File accountFile = new File("Account.txt");
        accountFile.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(accountFile, true));
        writer.write(this.account_id+","+this.user_name+","+this.ministry+","+this.initial_balance+","+this.balance+","+this.password);
        writer.flush();
        writer.newLine();
        writer.close();

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |             Success : you have created new account  with :         ACCOUNT ID       "+this.account_id+"       |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        writer.close();
        /*
                    SAVING THE TRANSACTION
         */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader reader= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = reader.readLine()) != null ){
            StringTokenizer st = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(st.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,"created new account",agentId);
        transaction.saveTransaction();
        BankSystemOptions.accountManagement();
    }


    public static void checkAccountBalance(String accountId,String password)throws Exception{
        String accountid,fullName,ministry,initBalance,balance;

        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        String records;
        while((records=reader.readLine())!= null ){
               if((records.contains(accountId) && (records.contains(password)))){
                   StringTokenizer st = new StringTokenizer(records,",");
                   accountid = st.nextToken();
                   fullName = st.nextToken();
                   ministry = st.nextToken();
                   initBalance = st.nextToken();
                   balance = st.nextToken();
                   System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                   System.out.println("\t\t\t\t\t\t\t |             Success: Wow , Authorized ,      VIEW YOUR ACCOUNT BALANCE                            |");
                   System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");

                   System.out.println("\n\n\t\t\t\t\t|-----------------------------------------------------------------------------------------------------------------|");
                   System.out.println("\t\t\t\t\t|                               "+fullName.toUpperCase()+"` S ACCOUNT DETAILS AND BALANCE                                       |");
                   System.out.println("\t\t\t\t\t|-----------------------------------------------------------------------------------------------------------------|");
                   System.out.println("\t\t\t\t\t|                        ACCOUNT ID         :                         "+accountid+"                                    |");
                   System.out.println("\t\t\t\t\t|                        FULL NAME          :                         "+fullName.toUpperCase()+"                                |");
                   System.out.println("\t\t\t\t\t|                        MINISTRY           :                         "+ministry+"                                     |");
                   System.out.println("\t\t\t\t\t|                        INITIAL BALANCE    :                         "+initBalance+"                                       |");
                   System.out.println("\t\t\t\t\t|                        CURRENT BALANCE    :                         "+balance+"                                       |");
                   System.out.println("\t\t\t\t\t|                                                                                                    !  success   |");
                   System.out.println("\t\t\t\t\t|-----------------------------------------------------------------------------------------------------------------|");

                   /*
                    SAVING THE TRANSACTION
                    */
                   File activeAgent = new File("ActiveAgent.txt");
                   BufferedReader read= new BufferedReader(new FileReader(activeAgent));
                   String agentRecord;
                   int agentId = 0;
                   while((agentRecord = read.readLine()) != null ){
                       StringTokenizer token = new StringTokenizer(agentRecord,",");
                       agentId = Integer.parseInt(token.nextToken());
                   }
                   Transaction transaction = new Transaction(null,null,0,0,"checked account balance",agentId);
                   transaction.saveTransaction();
                   BankSystemOptions.accountManagement();
               }
            }
        reader.close();
        }

    public static void upateAccount(String accountId,String password,String ministry,String firstName,String secondName)throws Exception{
        File accountFile = new File("Account.txt");
        File tempFile = new File("accountTemp.txt");
        tempFile.createNewFile();
        String records;
        String fullname = firstName+" "+secondName;

        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile, true));
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        while ((records = reader.readLine()) != null){
            StringTokenizer st= new StringTokenizer(records,",");
            if((records.contains(accountId) && (records.contains(password)))){
                writer.write(st.nextToken()+","+fullname+","+ministry+","+st.nextToken()+","+st.nextToken()+","+st.nextToken());
                writer.flush();
                writer.newLine();
            }
            else{
                writer.write(st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken());
                writer.flush();
                writer.newLine();
            }
        }

        writer.close();
        reader.close();

        accountFile.delete();
        tempFile.renameTo(accountFile);

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |             Success: You have updated the account : "+accountId+"  in Bank system                      |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                           /*
                    SAVING THE TRANSACTION
                    */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,"updated new account",agentId);
        transaction.saveTransaction();

        BankSystemOptions.accountManagement();
    }


    public static void deleteThisAccount(String accountId,String password)throws Exception{
        File accountFile = new File("Account.txt");
        File tempFile = new File("accountTemp.txt");
        tempFile.createNewFile();
        String records;

        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile, true));
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        while ((records = reader.readLine()) != null){
            StringTokenizer st= new StringTokenizer(records,",");
            if((records.contains(accountId) && (records.contains(password)))){
                //skip the found file data as deleting
               continue;
            }
            else{
                writer.write(st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken());
                writer.flush();
                writer.newLine();
            }
        }

        writer.close();
        reader.close();

        accountFile.delete();
        tempFile.renameTo(accountFile);

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |             Success: You have DELETED the account : "+accountId+"  in Bank system                      |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                                   /*
                    SAVING THE TRANSACTION
                    */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,"deleted an account",agentId);
        transaction.saveTransaction();

        BankSystemOptions.accountManagement();
    }

    public static void listOfAccount()throws Exception{
        int noAccount = 0;
        String accountId,fullname,ministry,balance,initBalance,password;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));
        System.out.println("\n\n\t\t\t|---------------------------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t|     NO         ACCOUNT ID                 FULL NAME                  BALANCE                MINISTRY                |");
            System.out.println("\t\t\t|---------------------------------------------------------------------------------------------------------------------|");
        String records;
        while ((records=reader.readLine())!= null ){
            noAccount ++;
            StringTokenizer st = new StringTokenizer(records,",");
            accountId = st.nextToken();
            fullname = st.nextToken();
            ministry = st.nextToken();
            initBalance = st.nextToken();
            balance = st.nextToken();
            password = st.nextToken();
            System.out.println("\t\t\t|     "+noAccount+"           "+accountId+"             "+fullname+"                   "+balance+"                 "+ministry+"                ");
        }
        System.out.println("\t\t\t|                                                                                                success : 0"+noAccount+" records |");
        System.out.println("\t\t\t|---------------------------------------------------------------------------------------------------------------------|");
        reader.close();
                                   /*
                    SAVING THE TRANSACTION
                    */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,"selected all account",agentId);
        transaction.saveTransaction();

        BankSystemOptions.accountManagement();
    }

    public static void listOfAccountByMinistry(String ministryToFind)throws Exception{
        int noAccount = 0;
        String accountId,fullname,ministry,balance,initBalance,password;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));
        System.out.println("\n\n\t\t\t|---------------------------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t|     NO         ACCOUNT ID                 FULL NAME                  BALANCE                MINISTRY                |");
        System.out.println("\t\t\t|---------------------------------------------------------------------------------------------------------------------|");
        String records;
        while ((records=reader.readLine())!= null ){
            if(records.contains(ministryToFind)){
                noAccount ++;
                StringTokenizer st = new StringTokenizer(records,",");
                accountId = st.nextToken();
                fullname = st.nextToken();
                ministry = st.nextToken();
                initBalance = st.nextToken();
                balance = st.nextToken();
                password = st.nextToken();
                System.out.println("\t\t\t|     "+noAccount+"           "+accountId+"             "+fullname+"                   "+balance+"                 "+ministry+"                ");
            }
        }
        System.out.println("\t\t\t|                                                                                                success : 0"+noAccount+" records |");
        System.out.println("\t\t\t|---------------------------------------------------------------------------------------------------------------------|");
        reader.close();
                                   /*
                    SAVING THE TRANSACTION
                    */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,"selected account by ministry",agentId);
        transaction.saveTransaction();


        BankSystemOptions.accountManagement();
    }
}