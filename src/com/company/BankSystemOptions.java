package com.company;
import java.util.Scanner;
public class BankSystemOptions {
    public static void accountManagement()throws Exception{
        Scanner scan = new Scanner(System.in);
        String choice;
        System.out.println("\n");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  ACCOUNT MANAGEMENT                   |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1           CREATE NEW ACCOUNT                                          |");
        System.out.println("\t\t\t\t|  2           CHECK BALANCE                                               |");
        System.out.println("\t\t\t\t|  3           UPDATE ACCOUNT SETTINGS                                     |");
        System.out.println("\t\t\t\t|  4           DELETING ACCOUNT SETTINGS                                   |");
        System.out.println("\t\t\t\t|  5           SELECTING ACCOUNT BY MINISTRY                               |");
        System.out.println("\t\t\t\t|  6           LIST OF ACCOUNTS                                            |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT                                                      |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                AccountManagement.addAccount();
                break;
            case "2":
                AccountManagement.checkBalance();
                break;
            case "3":
                AccountManagement.updateAccountSetting();
                break;
            case "4":
                AccountManagement.deleleteAccount();
                break;
            case "5":
                AccountManagement.listOfAccountByMinistry();
                break;
            case "6":
                AccountManagement.listOfAccount();
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                accountManagement();
        }

    }

    public static void withdrawManagement() throws Exception {
        Scanner scan = new Scanner(System.in);
        String accountId,password,choice;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  WITHDRAW MANAGEMENT                  |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1           WITHDRAW MONEY                                              |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                WithdrawManagement.withdrawMoneyOptions();
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                withdrawManagement();
        }
    }

    public static void depositManagement()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password,choice;
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  DEPOSIT MANAGEMENT                   |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1           INDIVIDUAL SAVING                                           |");
        System.out.println("\t\t\t\t|  2           JOINT ACCOUNTS                                              |");
        System.out.println("\t\t\t\t|  3           COMPANY & PARTNERSHIP ACCOUNT                               |");
        System.out.println("\t\t\t\t|  4           ACCOUNT OF GOVERNMENT                                       |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                DepositManagement.individualSaving();
                break;
            case "2":
                DepositManagement.jointSavingManagement();
                break;
            case "3":
                DepositManagement.companySavingManagement();
                break;
            case "4":
                WithdrawManagement.withdrawMoneyOptions();
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                withdrawManagement();
        }
    }

    public static void loanManagement()throws Exception{
        Scanner scan = new Scanner(System.in);
        String choice;
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   LOAN   MANAGEMENT                   |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1           SHORT TERM LOAN                                             |");
        System.out.println("\t\t\t\t|  2           INTERMEDIATE LOAN                                           |");
        System.out.println("\t\t\t\t|  3           LONG TERM LOAN                                              |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                LoanManagement.shortTermLoan();
                break;
            case "2":
                LoanManagement.intermediateLoan();
                break;
            case "3":
                LoanManagement.longTermCredit();
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                withdrawManagement();
        }
    }

    public static void foreignManagement()throws Exception {
        Scanner scan = new Scanner(System.in);
        String choice;
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   FOREIGN EXCHANGE                    |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1            MOBILE EXCHANGE                                            |");
        System.out.println("\t\t\t\t|  2            BANK UNIONS IN RWANDA                                      |");
        System.out.println("\t\t\t\t|  3            FOREIGN BANKS                                              |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                ForeignExchangeManagement.mobileExchange();
                break;
            case "2":
                ForeignExchangeManagement.bankUnionInRwanda();
                break;
            case "3":
                ForeignExchangeManagement.bankUnionInForiegnCountry();
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                withdrawManagement();
        }
    }
    public static void transactionManagement()throws Exception {
        Scanner scan = new Scanner(System.in);
        String choice;
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   TRANSACTION MANAGEMENT              |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1            ALL TRANSACTION MADE                                       |");
        System.out.println("\t\t\t\t|  2            SEARCH BY DATE                                             |");
        System.out.println("\t\t\t\t|  3            SEARCH BY AGENT ID                                         |");
        System.out.println("\t\t\t\t|  4            SEARCH BY ACCOUNT ID                                       |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                TransactionManagement.allTransaction();
                break;
            case "2":
                TransactionManagement.getTransactionByDate();
                break;
            case "3":
                TransactionManagement.getTransactionByAgentId();
                break;
            case "4":
                TransactionManagement.getTransactionByAccountId();
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                withdrawManagement();
        }
    }

    public static void bankBillingManagement(){

    }
}