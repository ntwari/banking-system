package com.company;
import java.io.*;
import java.util.StringTokenizer;

public class Agents {
    private int branch_id;
    private int login_id;
    private String name;
    private String email;
    private String password;
    private String user_name;

    public Agents(int branch_id, int login_id, String name, String email, String password, String user_name) {
        this.branch_id = branch_id;
        this.login_id = login_id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.user_name = user_name;
    }

    public int getBranch_id() {
        return this.branch_id;
    }

    public void setBranch_id(int branch_id) {
        this.branch_id = branch_id;
    }

    public int getLogin_id() {
        return this.login_id;
    }

    public void setLogin_id(int login_id) {
        this.login_id = login_id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_name() {
        return this.user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }


    public void saveAgent() throws IOException {
        File agentFile = new File("Agents.txt");
        agentFile.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(agentFile,true));

        writer.write(this.branch_id+","+this.login_id+","+this.name+","+this.email+","+this.user_name+","+this.password);
        writer.flush();
        writer.newLine();
        writer.close();

        System.out.println("\n\n\t\t|---------------------------------------------------------------------------|");
        System.out.println("\t\t|                                                                           |");
        System.out.println("\t\t|                NEW AGENT IS ADDED IN THE SYSTEM                           |");
        System.out.println("\t\t|                               |                                           |");
        System.out.println("\t\t|           he can do every bank operation assigned to him/her              |");
        System.out.println("\t\t|                                                                           |");
        System.out.println("\t\t| --------------------------------------------------------------------------|");

    }

    public  void getAllAgent() throws IOException {
        File agentFile = new File("Agents.txt");

        BufferedReader reader = new BufferedReader(new FileReader(agentFile));

        String records;
        int incrementingNumber = 0;

        System.out.println("\t\t'-------------------------------------------------------------------------------------------------------------------------------------'");
        System.out.println("\t\t|  NO        BRANCH ID    LOGIN ID        FULL NAME                 EMAIL                   USER NAME           PASSWORD              |");
        System.out.println("\t\t'-------------------------------------------------------------------------------------------------------------------------------------'");

        while((records=reader.readLine())!=null){
            incrementingNumber ++;

            StringTokenizer st = new StringTokenizer(records,",");
            System.out.println("\t\t    "+incrementingNumber+"\t\t    "+st.nextToken()+"\t\t  "+st.nextToken()+"\t\t "+st.nextToken()+"\t\t   "+st.nextToken()+"\t\t  "+st.nextToken()+"\t       "+st.nextToken());

        }
        System.out.println("\t\t'                                                                                                                                     '");
        System.out.println("\t\t'-------------------------------------------------------------------------------------------------------------------------------------'");
        reader.close();

    }
}