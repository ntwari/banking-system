package com.company;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
public class AccountManagement {
    public static void validateAuth(String accountId,String password)throws Exception{
        int incrementIfFound = 0;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        String records;
        while((records=reader.readLine())!= null ){
            if(records.contains(accountId) && (records.contains(password))) {
                incrementIfFound++;
            }
        }
        if(incrementIfFound ==  1){
        }
        else if(incrementIfFound == 0){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |             Warnings : Please account ID or password are incorrect                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            AccountManagement.checkBalance();
        }
        else{
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                      Error : Oops , System is down                                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            AccountManagement.checkBalance();
        }
    }
    public static void validateAccount(String ministry,String firstName,String secondName,String password,String passConfirm , Double initBal) throws Exception {
        if(ministry == null || firstName == null || secondName == null || password == null || passConfirm == null || initBal == null ){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                System.out.println("\t\t\t\t\t\t\t |             Warnings : Please all fields are required for registering new account                 |");
                System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                // back to the form
            addAccount();
        }

        else if(!(password.equals(passConfirm))){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                System.out.println("\t\t\t\t\t\t\t |             Warnings : Please password confirmation should be the same                            |");
                System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            // back to the form
            addAccount();
        }
        else{
            String username = firstName+ " "+secondName;
            Account account = new Account(password,username,initBal,ministry);
            account.saveAccount();
        }

    }
    public static void addAccount() throws Exception {
        Scanner scan = new Scanner(System.in);
        String ministry,firstName,secondName,password,passwordConfirmation;
        double initialBalance;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  CREATE NEW ACCOUNT                   |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER MINISTRY NAME        :    ");
        ministry = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER FIRST NAME           :    ");
        firstName = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER SECOND NAME          :    ");
        secondName = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER INITIAL BALANCE      :    ");
        initialBalance = scan.nextDouble();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.print  ("\t\t\t\t|    CONFIRM PASSWORD           :    ");
        passwordConfirmation = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        validateAccount(ministry,firstName,secondName,password,passwordConfirmation,initialBalance);
    }

    public static void checkBalance()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  LOGIN TO CHECK BALANCE               |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId,password);
        Account.checkAccountBalance(accountId,password);
    }

    public static void updateAccountSetting()throws Exception {
        Scanner scan = new Scanner(System.in);
        String accountId,password,ministry,firstName,secondName;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  LOGIN TO UPDATE                      |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        validateAuth(accountId,password);
        System.out.println("\n\n\n");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                           FILL THE FORM TO UPDATE                        |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t|    ENTER MINISTRY NAME        :    ");
        ministry = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER FIRST NAME           :    ");
        firstName = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER SECOND NAME          :    ");
        secondName = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");


        Account.upateAccount(accountId,password,ministry,firstName,secondName);
    }

    public static void deleleteAccount()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password,ministry,firstName,secondName,choice;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO DELETE THIS ACCOUNT               |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        validateAuth(accountId,password);
        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.print  ("\t\t\t\t\t\t\t |             Warnings : DO YOU WANT TO DELETE THIS ACCOUNT PAMANENTLY        y/n     :     ");
            choice = scan.next();
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        if(choice.equals("y")){
            Account.deleteThisAccount(accountId,password);
        }
        else{
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                System.out.println("\t\t\t\t\t\t\t |             Action : ACCOUNT IS NOT DELETED , Please continue transactions                        |");
                System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.accountManagement();
        }
    }

    public static void listOfAccount()throws Exception{
        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                    Success : THE LIST OF ACOUNTS IN DESTINY BANK                                  |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            Account.listOfAccount();
    }

    public static void listOfAccountByMinistry()throws Exception{
        Scanner scan = new Scanner(System.in);
        String ministry;
        System.out.println("\t\t\t\t\t|---------------------------------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t\t|      ENTER MINISTRY :                ");
        ministry = scan.next();
        System.out.println("\t\t\t\t\t|---------------------------------------------------------------------------------------------------|");

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                    Success : THE LIST OF ACOUNTS IN DESTINY BANK   OF MINISTRY                    |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        Account.listOfAccountByMinistry(ministry);
    }
}


