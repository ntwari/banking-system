package com.company;
import java.io.*;
import java.util.Date;
import java.util.Scanner;
import java.util.StringTokenizer;

public class BankSystemInitial  {
        public static void menuPage() throws Exception {
        Scanner scan = new Scanner(System.in);
        String optionChosen;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --  MENU PAGE                            |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1           ACCOUNT MANAGEMENT                                          |");
        System.out.println("\t\t\t\t|  2           WITHDRAWING MANAGEMENT                                      |");
        System.out.println("\t\t\t\t|  4           LOAN MANAGEMENT                                             |");
        System.out.println("\t\t\t\t|  5           FOREIGN MANAGEMENT                                          |");
        System.out.println("\t\t\t\t|  6           TRANSACTIONS MANAGEMENT                                     |");
        System.out.println("\t\t\t\t|  7           BANK BILLING MANAGEMENT                                     |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|--------------------------------------------------------------------------|\t\t : ");
        optionChosen = scan.next();

        switch (optionChosen){
            case "1":
                BankSystemOptions.accountManagement();
                break;
            case "2":
                BankSystemOptions.withdrawManagement();;
                break;
            case "3":
                BankSystemOptions.depositManagement();
                break;
            case "4":
                BankSystemOptions.loanManagement();
                break;
            case "5":
                BankSystemOptions.foreignManagement();
                break;
            case "6":
                BankSystemOptions.transactionManagement();
                break;
            case "7":
                BankSystemOptions.bankBillingManagement();
                break;
            case "100":
                loginPage();
                break;
            default:
                System.out.println(" Please you should choose from menu ");
                menuPage();
        }

    }

    public static void loginPage()  throws Exception {
        String agentID;
        String password;
        String agentRecords;
        int ifAgentFound = 0;

        Scanner scan = new Scanner(System.in);

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|       AGENT ID            :                                              |");
        agentID = scan.next();
        System.out.print  ("\t\t\t\t|       PASSWORD            :                                              |");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        File agentFile = new File("Agents.txt");
        File activeAgent= new File("ActiveAgent.txt");
        activeAgent.createNewFile();
        // it will overwrite the log in and save another login
        BufferedWriter writer = new BufferedWriter(new FileWriter(activeAgent));
        BufferedReader reader = new BufferedReader(new FileReader(agentFile));


        while((agentRecords=reader.readLine())!=null){
            if(agentRecords.contains(agentID) && agentRecords.contains(password)){
                StringTokenizer st = new StringTokenizer(agentRecords,",");
                ifAgentFound ++;
                Date today = new Date();
                writer.write(st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+st.nextToken()+","+today);
                writer.flush();
                writer.newLine();
                writer.close();
                System.out.println("\t\t\t\t\tAUTHORIZED ,\n\t\t =>>>> YOU MAY CONTINUE IN SYSTEM ");
                menuPage();
            }
        }

        if(ifAgentFound == 1){

        }
        else{
            System.out.println("\t\t PLEASE YOU NEED TO BE AUTHORIZED TO CONTINUE IN THE SYSTEM ");
            loginPage();
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("\n\n");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|                           DESTINY BANK                                   |");
        System.out.println("\t\t\t\t|                                 *                                        |");
        System.out.println("\t\t\t\t|                           BANKING SYSTEM                                 |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        loginPage();
    }
}

//20194201
//ntpass@123