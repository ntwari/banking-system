package com.company;

public class BankSettings {
    private int bankId;
    private double allStoredMoney;
    private String managerName;
    /*
            TYPE OF BANK
            -> Bank
            -> Branch
     */
    private String typeOfBank;

    public BankSettings(int bankId, double allStoredMoney, String managerName, String typeOfBank) {
        this.bankId = bankId;
        this.allStoredMoney = allStoredMoney;
        this.managerName = managerName;
        this.typeOfBank = typeOfBank;
    }

    public int getBankId() {
        return this.bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public double getAllStoredMoney() {
        return this.allStoredMoney;
    }

    public void setAllStoredMoney(double allStoredMoney) {
        this.allStoredMoney = allStoredMoney;
    }

    public String getManagerName() {
        return this.managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getTypeOfBank() {
        return this.typeOfBank;
    }

    public void setTypeOfBank(String typeOfBank) {
        this.typeOfBank = typeOfBank;
    }
}