package com.company;
import java.io.*;
import java.util.*;

public class Loan {
    private int debtorAccountId;
    private int creditorAccountId; // default it is the bank account
    private Double initialLoan;
    private Double interestRate;
    private Double totalLoan;
    private Double collatorSecurityPrice;  // agaciro k ingwate
    private String loanType;   // loan purpase
    private Date dateOfBorrowing;
    private Date dateOfExpiry;
    private String finished; // if the loan is finished to be paid it must be 0 nor 1

    public Loan(int creditorAccountId, double initialLoan, double interestRate, double totalLoan, Double correllatorySecurityPrice, String loanType, Date dateOfBorrowing, Date dateOfExpiry) {
        this.creditorAccountId = creditorAccountId;
        this.initialLoan = initialLoan;
        this.interestRate = interestRate;
        this.totalLoan = totalLoan;
        this.collatorSecurityPrice = correllatorySecurityPrice;
        this.loanType = loanType;
        this.dateOfBorrowing = dateOfBorrowing;
        this.dateOfExpiry = dateOfExpiry;
        this.finished = "not finished";
    }

    public void saveLoan()throws Exception {
        File laonFile = new File("Loan.txt");
        laonFile.createNewFile();

        BufferedWriter  writer = new BufferedWriter(new FileWriter(laonFile,true));
        writer.write(this.creditorAccountId+","+this.interestRate+","+this.initialLoan+","+this.totalLoan+","+this.collatorSecurityPrice+","+this.loanType+","+this.dateOfBorrowing+","+this.dateOfExpiry+","+this.finished);
        writer.flush();
        writer.newLine();
        writer.close();
                System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                System.out.println("\t\t\t\t\t\t\t |             Success :       You have given the loan remember to pay it back on time               |");
                System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(String.valueOf(this.creditorAccountId),String.valueOf(this.debtorAccountId),0,this.initialLoan,"loan is given to the account ",agentId);
        transaction.saveTransaction();

        BankSystemOptions.loanManagement();
    }

    public int getDebtorAccountId() {
        return this.debtorAccountId;
    }

    public void setDebtorAccountId(int debtorAccountId) {
        this.debtorAccountId = debtorAccountId;
    }

    public int getCreditorAccountId() {
        return this.creditorAccountId;
    }

    public void setCreditorAccountId(int creditorAccountId) {
        this.creditorAccountId = creditorAccountId;
    }

    public double getInitialLoan() {
        return this.initialLoan;
    }

    public void setInitialLoan(double initialLoan) {
        this.initialLoan = initialLoan;
    }

    public double getInterestRate() {
        return this.interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getTotalLoan() {
        return this.totalLoan;
    }

    public void setTotalLoan(double totalLoan) {
        this.totalLoan = totalLoan;
    }

    public double getCollatorSecurityPrice() {
        return this.collatorSecurityPrice;
    }

    public void setCollatorSecurityPrice(double collatorSecurityPrice) {
        this.collatorSecurityPrice = collatorSecurityPrice;
    }

    public String getLoanType() {
        return this.loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public Date getDateOfBorrowing() {
        return this.dateOfBorrowing;
    }

    public void setDateOfBorrowing(Date dateOfBorrowing) {
        this.dateOfBorrowing = dateOfBorrowing;
    }

    public Date getDateOfExpiry() {
        return this.dateOfExpiry;
    }

    public void setDateOfExpiry(Date dateOfExpiry) {
        this.dateOfExpiry = dateOfExpiry;
    }
}
