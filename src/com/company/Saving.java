package com.company;
        import java.io.*;
        import java.util.StringTokenizer;

public class Saving {
    private int accountToSave;
    private double interestRate = 5;
    private double savedMoney;
    private String typeOfSavings;
    private double currentMoney;
    private String source; // from balance , currently coming , micro-finance money

    public Saving(int accountToSave, double savedMoney, String source) throws Exception {
        this.accountToSave = accountToSave;
        this.savedMoney = savedMoney;
        this.source = source;
        this.currentMoney = this.savedMoney;
    }
    public void individualSaving()throws Exception {
        this.typeOfSavings = "individual";
        File savingFile = new File("Saving.txt");
        savingFile.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(savingFile));
        writer.write(this.accountToSave+","+this.interestRate+","+this.savedMoney+","+this.currentMoney+","+this.source+","+this.typeOfSavings);
        writer.flush();
        writer.newLine();
        writer.close();

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t\t\t\t |             Success :       saving done now you are going to start getting interest               |");
        System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(String.valueOf(this.accountToSave),null,0,0,"saved money to account individual",agentId);
        transaction.saveTransaction();

        BankSystemOptions.depositManagement();
    }

    public void jointSaving()throws Exception {
        this.typeOfSavings = "joint";
        File savingFile = new File("Saving.txt");
        savingFile.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(savingFile,true));
        writer.write(this.accountToSave+","+this.interestRate+","+this.savedMoney+","+this.currentMoney+","+this.source+","+this.typeOfSavings);
        writer.flush();
        writer.newLine();
        writer.close();

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t\t\t\t |             Success :     saving done now you are going to start getting interest which is equal  |");
        System.out.println("\t\t\t\t\t\t\t |                           The withdraw of money will be made by you all                           |");
        System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(String.valueOf(this.accountToSave),null,0,savedMoney,"saved money to account joint",agentId);
        transaction.saveTransaction();

        BankSystemOptions.depositManagement();
    }
    public void companySaving() throws Exception {
        this.typeOfSavings = "company";
        File savingFile = new File("Saving.txt");
        savingFile.createNewFile();

        BufferedWriter writer = new BufferedWriter(new FileWriter(savingFile));
        writer.write(this.accountToSave+","+this.interestRate+","+this.savedMoney+","+this.currentMoney+","+this.source+","+this.typeOfSavings);
        writer.flush();
        writer.newLine();
        writer.close();

        System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t\t\t\t |             Success :       saving done now you are going to start getting interest               |");
        System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
        /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(String.valueOf(this.accountToSave),null,0,0,"saved money to account as company",agentId);
        transaction.saveTransaction();

        BankSystemOptions.depositManagement();
    }

    public String getTypeOfSavings() {
        return typeOfSavings;
    }

    public void setTypeOfSavings(String typeOfSavings) {
        this.typeOfSavings = typeOfSavings;
    }

    public int getAccountToSave() {
        return this.accountToSave;
    }

    public void setAccountToSave(int accountToSave) {
        this.accountToSave = accountToSave;
    }

    public double getInterestRate() {
        return this.interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getSavedMoney() {
        return this.savedMoney;
    }

    public void setSavedMoney(double savedMoney) {
        this.savedMoney = savedMoney;
    }

    public double getCurrentMoney() {
        return this.currentMoney;
    }

    public void setCurrentMoney(double currentMoney) {
        this.currentMoney = currentMoney;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}