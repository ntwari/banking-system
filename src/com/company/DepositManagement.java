package com.company;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
import java.util.StringTokenizer;

public class DepositManagement {
    public static void validateAuth(String accountId,String password,String typeOfBank)throws Exception{
        int incrementIfFound = 0;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        String records;
        while((records=reader.readLine())!= null ){
            if(records.contains(accountId) && (records.contains(password)) && (records.contains(typeOfBank))) {
                incrementIfFound++;
                /*
                SAVING THE TRANSACTION
                 */
                File activeAgent = new File("ActiveAgent.txt");
                BufferedReader read= new BufferedReader(new FileReader(accountFile));
                String agentRecord;
                int agentId = 0;
                while((agentRecord = read.readLine()) != null ){
                    StringTokenizer token = new StringTokenizer(agentRecord,",");
                    agentId = Integer.parseInt(token.nextToken());
                }
                Transaction transaction = new Transaction(accountId,null,0,0,"authentication done to account",agentId);
                transaction.saveTransaction();
            }
        }
        if(incrementIfFound ==  1){
        }
        else if(incrementIfFound == 0){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                System.out.println("\t\t\t\t\t\t\t |        Warnings : Please is this your account ? no make sure this is correct credentials          |");
                System.out.println("\t\t\t\t\t\t\t |                   Or make sure this is not account of group , cooperative and company             |");;
                System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.depositManagement();
        }
        else{
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                      Error : Oops , System is down                                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.depositManagement();
        }
    }

    public static void individualSaving()throws Exception {
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        int AccountID;
        Double moneyToDeposit;
        Double moneyToWithDraw;
        String choice;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO DEPOSIT TO     ACCOUNT            |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId,password,"individual");
        System.out.print  ("\t\t\t\t|    ENTER MONEY TO DEPOSIT      :    ");
        moneyToDeposit = scan.nextDouble();
        System.out.println("\t\t\t\t|    CHOOSE THE SOURCE           :    1.   BALANCE ");
        System.out.println("\t\t\t\t|                                :    2.   CURRENT COMING ");
        System.out.println("\t\t\t\t|                                :    3.   FROM BANK ");
        choice = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        AccountID = Integer.parseInt(accountId);
        moneyToWithDraw = moneyToDeposit;

        switch (choice){
            case "1":
                WithdrawManagement.withdrawMoney(accountId,password,moneyToWithDraw);
                Saving saving = new Saving(AccountID,moneyToDeposit,"balance");
                saving.individualSaving();
                break;
            case "2":
                Saving currentSaving = new Saving(AccountID,moneyToDeposit,"current coming");
                currentSaving.individualSaving();
                break;
            case "3":
                System.out.println("\t\t\tAPIS of the bank are not yet published in the system");
                break;
            default:
                System.out.println("\t\t\tPlease you should enter the valid input of your source of saving");
                individualSaving();
        }
    }

    public static void jointSavingManagement()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        int AccountID;
        Double moneyToDeposit;
        Double moneyToWithDraw;
        String choice;
        int numberOfJoints = 0;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t|    ENTER MONEY TO DEPOSIT                 :    ");
        moneyToDeposit = scan.nextDouble();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t|   HOW MANY PEOPLE TO JOIN THEIR ACCOUNT   :    ");
        numberOfJoints = scan.nextInt();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        for (int i = 1 ; i <= numberOfJoints ; i++){
            System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO DEPOSIT  TO  "+i+" ACCOUNT            |");
            System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t|                                                                          |");
            System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
            accountId = scan.next();
            System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
            password = scan.next();
            System.out.println("\t\t\t\t|                                                                          |");
            System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
            validateAuth(accountId,password,"individual");
            System.out.println("\t\t\t\t|    CHOOSE THE SOURCE           :    1.   CURRENT COMING  ");
            System.out.println("\t\t\t\t|                                :    2.   FROM BANK ");
            choice = scan.next();
            System.out.println("\t\t\t\t|                                                                          |");
            System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
            AccountID = Integer.parseInt(accountId);
            moneyToWithDraw = moneyToDeposit;

            switch (choice){
                case "1":
                    Saving currentSaving = new Saving(AccountID,moneyToDeposit,"current coming");
                    currentSaving.jointSaving();
                    break;
                case "2":
                    System.out.println("\t\t\tAPIS of the bank are not yet published in the system");
                    break;
                default:
                    System.out.println("\t\t\tPlease you should enter the valid input of your source of saving");
                    individualSaving();
            }
        }
    }

    public static void companySavingManagement() throws Exception {
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        int AccountID;
        Double moneyToDeposit;
        Double moneyToWithDraw;
        String choice;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO DEPOSIT TO COMPANY ACCOUNT        |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId,password,"company");
        System.out.print  ("\t\t\t\t|    ENTER MONEY TO DEPOSIT      :    ");
        moneyToDeposit = scan.nextDouble();
        System.out.println("\t\t\t\t|    CHOOSE THE SOURCE           :    1.   BALANCE ");
        System.out.println("\t\t\t\t|                                :    2.   CURRENT COMING ");
        System.out.println("\t\t\t\t|                                :    3.   FROM BANK ");
        choice = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        AccountID = Integer.parseInt(accountId);
        moneyToWithDraw = moneyToDeposit;

        switch (choice){
            case "1":
                WithdrawManagement.withdrawMoney(accountId,password,moneyToWithDraw);
                Saving saving = new Saving(AccountID,moneyToDeposit,"balance");
                saving.companySaving();
                break;
            case "2":
                Saving currentSaving = new Saving(AccountID,moneyToDeposit,"current coming");
                currentSaving.companySaving();
                break;
            case "3":
                System.out.println("\t\t\tAPIS of the bank are not yet published in the system");
                break;
            default:
                System.out.println("\t\t\tPlease you should enter the valid input of your source of saving");
                individualSaving();
        }
    }
}