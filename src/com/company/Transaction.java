package com.company;

import java.io.*;
import java.util.Date;
import java.util.*;

public class Transaction {
    private String senders_account;
    private String receivers_account;
    private double fee_payable;
    private double amount_in_transaction;
    private int hashcode;
    private String actionDone;
    private int agent_id;
    private Date today;

    public Transaction(String senders_account, String receivers_account, double fee_payable, double amount_in_transaction, String actionDone, int agent_id) {
        this.senders_account = senders_account;
        this.receivers_account = receivers_account;
        this.fee_payable = fee_payable;
        this.amount_in_transaction = amount_in_transaction;
        this.actionDone = actionDone;
        this.agent_id = agent_id;
    }

    public void saveTransaction()throws Exception {
        this.today = new Date();

        File transactionFile = new File("Transaction.txt");
        transactionFile.createNewFile();
        BufferedWriter writer = new BufferedWriter(new FileWriter(transactionFile,true));
        writer.write(this.senders_account+","+this.receivers_account+","+this.fee_payable+","+this.amount_in_transaction+","+this.actionDone+","+this.agent_id+","+this.today);
        writer.flush();
        writer.newLine();
        writer.close();

        System.out.println("\n\n\t\t|---------------------------------------------------------------------------|");
            System.out.println("\t\t|               Transaction is done and saved in its file                   |");
            System.out.println("\t\t| --------------------------------------------------------------------------|");
    }

    public static void getAllTransaction()throws Exception{
        File transactionFile = new File("Transaction.txt");
        BufferedReader reader = new BufferedReader(new FileReader(transactionFile));
        int transactionId = 0;
        String records;
//        1138109975,11992923,0.0,800.0,foreign exchange,20194201,Fri Apr 24 22:56:18 CAT 2020
        System.out.println("\n\n\n");
        System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|  No     sender`s account   receiver`s account   fee payed     money/frw   type of transaction      Doneby /AgentID                 occurred at            |");
        System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
        while((records = reader.readLine()) != null ){
            StringTokenizer st = new StringTokenizer(records,",");
            transactionId ++;
            System.out.println("\t\t\t\t|  "+transactionId+"       "+st.nextToken()+"              "+st.nextToken()+"         "+st.nextToken()+"          "+st.nextToken()+"         "+st.nextToken()+"         "+st.nextToken()+"             "+st.nextToken());
        }
        System.out.println("\t\t\t\t|                                                                                                                                 success: "+transactionId+" transactions  |");
        System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
        reader.close();
    }

    public static  void searchingTransaction(String searchBy)throws Exception{
        File transactionFile = new File("Transaction.txt");
        BufferedReader reader = new BufferedReader(new FileReader(transactionFile));
        int transactionId = 0,found = 0;
        String records;
        System.out.println("\n\n\n");
        System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|  No     sender`s account   receiver`s account   fee payed     money/frw   type of transaction      Doneby /AgentID                 occurred at            |");
        System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
        while((records = reader.readLine()) != null ){
            StringTokenizer st = new StringTokenizer(records,",");
            if(records.contains(searchBy)){
                found ++;
                transactionId ++;
                System.out.println("\t\t\t\t|  "+transactionId+"       "+st.nextToken()+"              "+st.nextToken()+"         "+st.nextToken()+"          "+st.nextToken()+"         "+st.nextToken()+"         "+st.nextToken()+"             "+st.nextToken());
            }
        }

        if(found == 0){
            System.out.println("\t\t\t\t|----------------                             Warning :   NO TRANSACTION IS FOUND                         --------------------------------------------------|");
            System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
            BankSystemOptions.transactionManagement();
        }
        System.out.println("\t\t\t\t|                                                                                                                                 success: "+transactionId+" transactions  |");
        System.out.println("\t\t\t\t|-----------------------------------------------------------------------------------------------------------------------------------------------------------|");
        reader.close();
    }
    public String getSenders_account() {
        return senders_account;
    }

    public void setSenders_account(String senders_account) {
        this.senders_account = senders_account;
    }

    public String getReceivers_account() {
        return receivers_account;
    }

    public void setReceivers_account(String receivers_account) {
        this.receivers_account = receivers_account;
    }

    public double getFee_payable() {
        return fee_payable;
    }

    public void setFee_payable(double fee_payable) {
        this.fee_payable = fee_payable;
    }

    public double getAmount_in_transaction() {
        return amount_in_transaction;
    }

    public void setAmount_in_transaction(double amount_in_transaction) {
        this.amount_in_transaction = amount_in_transaction;
    }

    public int getHashcode() {
        return hashcode;
    }

    public void setHashcode(int hashcode) {
        this.hashcode = hashcode;
    }

    public String getActionDone() {
        return actionDone;
    }

    public void setActionDone(String actionDone) {
        this.actionDone = actionDone;
    }

    public int getAgent_id() {
        return agent_id;
    }

    public void setAgent_id(int agent_id) {
        this.agent_id = agent_id;
    }
}