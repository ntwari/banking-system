package com.company;
import java.io.*;
import java.util.StringTokenizer;

public class ForeignExchange {
    private int receiversAccountId;
    private double moneyInRwf;
    private String foreignExchangeKey; // dollar , pound , kenyan dollar ,euro
    private double moneyTransfered; // money just transfered in the transaction
    private String forignBankName ; // in capital
    private int sendersAccountId;
    private String madeBy ;  // { bank , beneficiary }
    private String countryName;

    public ForeignExchange(int sendersAccountId, double moneyInRwf, String foreignExchangeKey, double moneyTransfered, String forignBankName, int receiversAccountId, String madeBy ,String countryName) {
        this.receiversAccountId = receiversAccountId;
        this.moneyInRwf = moneyInRwf;
        this.foreignExchangeKey = foreignExchangeKey;
        this.moneyTransfered = moneyTransfered;
        this.forignBankName = forignBankName;
        this.sendersAccountId = sendersAccountId;
        this.madeBy = madeBy;
        this.countryName = countryName;
    }

    public void saveForeignExchange()throws Exception{
        File foreginExchangeFile = new File("ForeignExchange.txt");
        foreginExchangeFile.createNewFile();
        if(!(forignBankName.equals("rwanda"))){
            moneyTransfered = moneyInRwf / 960;
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(foreginExchangeFile,true));
        writer.write(this.sendersAccountId+","+this.receiversAccountId+","+this.moneyInRwf+","+moneyTransfered+","+this.madeBy+","+this.forignBankName+","+this.countryName);
        writer.flush();
        writer.newLine();
        writer.close();
        System.out.println("\n\n\t\t\t\t\t\t\t |------------------------------------------------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t\t\t\t |         Success :   You have transferred money in foreign account  from "+sendersAccountId+"   to  "+receiversAccountId+"                   |");
        System.out.println("\t\t\t\t\t\t\t |------------------------------------------------------------------------------------------------------------------|");
        /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(String.valueOf(this.sendersAccountId),String.valueOf(this.receiversAccountId),0,this.moneyInRwf,"foreign exchange",agentId);
        transaction.saveTransaction();
        BankSystemOptions.foreignManagement();
    }

    public int getReceiversAccountId() {
        return getReceiversAccountId();
    }   

    public void setReceiversAccountId(int receiversAccountId) {
        this.receiversAccountId = receiversAccountId;
    }

    public double getMoneyInRwf() {
        return this.moneyInRwf;
    }

    public void setMoneyInRwf(double moneyInRwf) {
        this.moneyInRwf = moneyInRwf;
    }

    public String getForeignExchangeKey() {
        return this.foreignExchangeKey;
    }

    public void setForeignExchangeKey(String foreignExchangeKey) {
        this.foreignExchangeKey = foreignExchangeKey;
    }

    public double getMoneyTransfered() {
        return this.moneyTransfered;
    }

    public void setMoneyTransfered(double moneyTransfered) {
        this.moneyTransfered = moneyTransfered;
    }

    public String getForignBankName() {
        return this.forignBankName;
    }

    public void setForignBankName(String forignBankName) {
        this.forignBankName = forignBankName;
    }

    public int getSendersAccountId() {
        return this.sendersAccountId;
    }

    public void setSendersAccountId(int sendersAccountId) {
        this.sendersAccountId = sendersAccountId;
    }

    public String getMadeBy() {
        return this.madeBy;
    }

    public void setMadeBy(String madeBy) {
        this.madeBy = madeBy;
    }
}
