package com.company;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
public class ForeignExchangeManagement {
    public static void validateAuth(String accountId,String password)throws Exception{
        int incrementIfFound = 0;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        String records;
        while((records=reader.readLine())!= null ){
            if(records.contains(accountId) && (records.contains(password))) {
                incrementIfFound++;
            }
        }
        if(incrementIfFound ==  1){
        }
        else if(incrementIfFound == 0){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |        Warnings : Please is this your account ? no make sure this is correct credentials          |");
            System.out.println("\t\t\t\t\t\t\t |                   In order to make loan syndication you need to be authenticated in system        |");;
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.foreignManagement();
        }
        else{
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                      Error : Oops , System is down                                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.foreignManagement();
        }
    }

    public static void mobileExchange() throws Exception {
        Scanner scan = new Scanner(System.in);
        String choice,mobileCompany;
        Double moneyToTransfer;

        String accountId, password, telephonenumber;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   MOBILE EXCHANGE                     |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1            AIRTEL  MONEY & TIGO CASH                                  |");
        System.out.println("\t\t\t\t|  2            MTN MOBILE MONEY                                           |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO GET LONG TERM LOAN                |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId, password);
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print("\t\t\t\t|    ENTER MONEY TO DRIVE   :      ");
        moneyToTransfer = scan.nextDouble();
        System.out.print("\t\t\t\t|    ENTER PHONE NUMBER     :      ");
        telephonenumber = scan.next();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        switch (choice) {
            case "1":
                if ((telephonenumber.startsWith("072")) || (telephonenumber.startsWith("073"))) {
                    int telephone = Integer.parseInt(telephonenumber);
                    int Account = Integer.parseInt(accountId);
                    mobileCompany = "airtel & tigo";
                    WithdrawManagement.withdrawMoney(accountId,password,moneyToTransfer);
                    ForeignExchange mobileExchange = new ForeignExchange(Account,moneyToTransfer,null,moneyToTransfer,mobileCompany,telephone,"DESTINY BANK","rwanda");
                    mobileExchange.saveForeignExchange();
                } else {
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    System.out.println("\t\t\t\t\t\t\t |         Warning : In order to send money to mobile money you should use tigo or airtel number     |");
                    System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    mobileExchange();
                }
                break;
            case "2":
                if (telephonenumber.contains("078")) {
                    int telephone = Integer.parseInt(telephonenumber);
                    int Account = Integer.parseInt(accountId);
                    mobileCompany = "mtn";
                    WithdrawManagement.withdrawMoney(accountId,password,moneyToTransfer);
                    ForeignExchange mobileExchange = new ForeignExchange(Account,moneyToTransfer,null,moneyToTransfer,mobileCompany,telephone,"DESTINY BANK","rwanda");
                    mobileExchange.saveForeignExchange();
                } else {
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    System.out.println("\t\t\t\t\t\t\t |         Warning : In order to send money to mobile money you should use MTN number                |");
                    System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    mobileExchange();
                }
                break;
            case "#":
                BankSystemInitial.menuPage();
                break;
            case "100":
                BankSystemInitial.loginPage();
                break;
            default:
                System.out.println("\t\t\tOops , you entered invalid choice ");
                mobileExchange();
        }
    }

    public static void bankUnionInRwanda()throws Exception {
        Scanner scan = new Scanner(System.in);
        String choice,foreinBank = null;
        Double moneyToTransfer;

        String accountId, password,receiversAccount;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   FOREIGN EXCHANGE                    |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1            AB BANK LIMITED                                            |");
        System.out.println("\t\t\t\t|  2            ACCESS BANK LIMITED                                        |");
        System.out.println("\t\t\t\t|  3            BANK OF KIGALI LIMITED                                     |");
        System.out.println("\t\t\t\t|  4            BPR BANK LIMITED                                           |");
        System.out.println("\t\t\t\t|  5            COGEBAQUE LIMITED                                          |");
        System.out.println("\t\t\t\t|  6            CRANE BANK LIMITED                                         |");
        System.out.println("\t\t\t\t|  7            ECHO BANK LIMITED                                          |");
        System.out.println("\t\t\t\t|  8            EQUITY BANK LIMITED                                        |");
        System.out.println("\t\t\t\t|  9            I&M BANK LIMITED                                           |");
        System.out.println("\t\t\t\t|  10           GT BANK LIMITED                         N                  |");
        System.out.println("\t\t\t\t|  11           KCB BANK LIMITED                                           |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                foreinBank = "AB BANK";
                break;
            case "2":
                foreinBank = "ACCESS BANK";
                break;
            case "3":
                foreinBank = "BK BANK";
                break;
            case "4":
                foreinBank = "BPR BANK";
                break;
            case "5":
                foreinBank = "COGEBANQUE";
                break;
            case "9":
                foreinBank = "I&M BANK";
                break;
            case "7":
                foreinBank = "ECHO BANK";
                break;
            case "8":
                foreinBank = "EQUITY BANK";
                break;
            case "10":
                foreinBank = "GT BANK";
                break;
            case "11":
                foreinBank = "KCB BANK";
                break;
            default:
                System.out.println("\t\t\t\t  You should choose the foreign bak available ");
        }

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO GET LONG TERM LOAN                |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId, password);
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER MONEY TO DRIVE   :      ");
        moneyToTransfer = scan.nextDouble();
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID       :      ");
        receiversAccount = scan.next();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        int Account = Integer.parseInt(accountId);
        int receivers = Integer.parseInt(receiversAccount);
        WithdrawManagement.withdrawMoney(accountId,password,moneyToTransfer);
        ForeignExchange mobileExchange = new ForeignExchange(Account,moneyToTransfer,null,moneyToTransfer,foreinBank,receivers,"DESTINY BANK","rwanda");
        mobileExchange.saveForeignExchange();

    }

    public static void bankUnionInForiegnCountry()throws Exception {
        Scanner scan = new Scanner(System.in);
        String choice,foreinBank = null,foreignCountry =  null;
        Double moneyToTransfer;

        String accountId, password,receiversAccount;
    /*
                    FOREIGN BANK OF OPERATION
     */
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   FOREIGN EXCHANGE                    |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1            AB BANK LIMITED                                            |");
        System.out.println("\t\t\t\t|  2            ACCESS BANK LIMITED                                        |");
        System.out.println("\t\t\t\t|  3            BANK OF KIGALI LIMITED                                     |");
        System.out.println("\t\t\t\t|  4            BPR BANK LIMITED                                           |");
        System.out.println("\t\t\t\t|  5            COGEBAQUE LIMITED                                          |");
        System.out.println("\t\t\t\t|  6            CRANE BANK LIMITED                                         |");
        System.out.println("\t\t\t\t|  7            ECHO BANK LIMITED                                          |");
        System.out.println("\t\t\t\t|  8            EQUITY BANK LIMITED                                        |");
        System.out.println("\t\t\t\t|  9            I&M BANK LIMITED                                           |");
        System.out.println("\t\t\t\t|  10           GT BANK LIMITED                         N                  |");
        System.out.println("\t\t\t\t|  11           KCB BANK LIMITED                                           |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                foreinBank = "AB BANK";
                break;
            case "2":
                foreinBank = "ACCESS BANK";
                break;
            case "3":
                foreinBank = "BK BANK";
                break;
            case "4":
                foreinBank = "BPR BANK";
                break;
            case "5":
                foreinBank = "COGEBANQUE";
                break;
            case "9":
                foreinBank = "I&M BANK";
                break;
            case "7":
                foreinBank = "ECHO BANK";
                break;
            case "8":
                foreinBank = "EQUITY BANK";
                break;
            case "10":
                foreinBank = "GT BANK";
                break;
            case "11":
                foreinBank = "KCB BANK";
                break;
            default:
                System.out.println("\t\t\t\t  You should choose the foreign bank available ");
        }
    /*
                    FOREIGN COUNTRY OF OPERATION
     */
        System.out.println("\n\n\n\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                   DESTINY BANK  --   CHOOSE COUNTRY                      |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  1            KENYA                                                      |");
        System.out.println("\t\t\t\t|  2            UGANDA                                                     |");
        System.out.println("\t\t\t\t|  3            TANZANIA                                                   |");
        System.out.println("\t\t\t\t|  4            ZAMBIA                                                     |");
        System.out.println("\t\t\t\t|  5            DRC                                                        |");
        System.out.println("\t\t\t\t|  6            NIGER                                                      |");
        System.out.println("\t\t\t\t|  7            CONGO BRAZZAVILLE                                          |");
        System.out.println("\t\t\t\t|  8            GHANA                                                      |");
        System.out.println("\t\t\t\t|  9            MALAWI                                                     |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|  #           BACK TO MENU PAGE                                           |");
        System.out.println("\t\t\t\t|  100         LOGOUT FROM SYSTEM                                          |");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        choice = scan.next();

        switch (choice){
            case "1":
                foreignCountry = "KENYA";
                break;
            case "2":
                foreignCountry = "UGANDA";
                break;
            case "3":
                foreignCountry = "TANZANIA";
                break;
            case "4":
                foreignCountry = "ZAMBIA";
                break;
            case "5":
                foreignCountry = "DRC";
                break;
            case "6":
                foreignCountry = "NIGER";
                break;
            case "7":
                foreignCountry = "CONGO BRAZZAVILLE";
                break;
            case "8":
                foreignCountry = "GHANA";
                break;
            case "9":
                foreignCountry = "MALAWI";
                break;
            default:
                System.out.println("\t\t\t\t  You should choose the foreign country available ");
        }


        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO GET LONG TERM LOAN                |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateAuth(accountId, password);
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER MONEY TO DRIVE   :      ");
        moneyToTransfer = scan.nextDouble();
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID       :      ");
        receiversAccount = scan.next();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        int Account = Integer.parseInt(accountId);
        int receivers = Integer.parseInt(receiversAccount);
        WithdrawManagement.withdrawMoney(accountId,password,moneyToTransfer);
        ForeignExchange mobileExchange = new ForeignExchange(Account,moneyToTransfer,"dollar",moneyToTransfer,foreinBank,receivers,"DESTINY BANK",foreignCountry);
        mobileExchange.saveForeignExchange();
    }
}