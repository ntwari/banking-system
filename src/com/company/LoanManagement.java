package com.company;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;
import java.util.Scanner;
import java.util.StringTokenizer;

public class LoanManagement {
    public static void validateAuth(String accountId,String password)throws Exception{
        int incrementIfFound = 0;
        File accountFile = new File("Account.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));

        String records;
        while((records=reader.readLine())!= null ){
            if(records.contains(accountId) && (records.contains(password))) {
                incrementIfFound++;
            }
        }
        if(incrementIfFound ==  1){
        }
        else if(incrementIfFound == 0){
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |        Warnings : Please is this your account ? no make sure this is correct credentials          |");
            System.out.println("\t\t\t\t\t\t\t |                   In order to make loan syndication you need to be authenticated in system        |");;
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.loanManagement();
        }
        else{
            System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            System.out.println("\t\t\t\t\t\t\t |                      Error : Oops , System is down                                                |");
            System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
            BankSystemOptions.loanManagement();
        }
    }
    public static void validateInSaving(String accountId,Double amountToBorrower,String kind)throws Exception
    {
        File accountFile = new File("Saving.txt");
        BufferedReader reader = new BufferedReader(new FileReader(accountFile));
        String AccountId,rate,initSaving,currSaving;
        String records;
        while((records=reader.readLine())!= null ){
            if(records.contains(accountId)) {
                StringTokenizer st = new StringTokenizer(records,",");
                AccountId = st.nextToken();
                rate = st.nextToken();
                initSaving = st.nextToken();
                currSaving = st.nextToken();
                Double allowedSaving = Double.parseDouble(currSaving);

                if((amountToBorrower < (allowedSaving * 2)) && (kind == "short")){ // you should not lend money greater to your saving * 2 as short term credit

                }
                else if((amountToBorrower > (allowedSaving * 2)) && (kind == "short")){
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    System.out.println("\t\t\t\t\t\t\t |        Warnings : Please the money you allowed to credit as short term credit                  |");
                    System.out.println("\t\t\t\t\t\t\t |                   should be less than or equal to "+(allowedSaving*2)+"                                      |");;
                    System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    shortTermLoan();
                }
                else if((amountToBorrower < (allowedSaving * 5)) && (kind == "intermediate")){

                }
                else if((amountToBorrower > (allowedSaving * 5)) && (kind == "intermediate")){
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    System.out.println("\t\t\t\t\t\t\t |        Warnings : Please the money you allowed to credit as short term credit                  |");
                    System.out.println("\t\t\t\t\t\t\t |                   should be less than or equal to "+(allowedSaving*5)+"                                      |");;
                    System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    intermediateLoan();
                }

                else if((amountToBorrower < (allowedSaving * 15)) && (kind == "long")){

                }
                else if((amountToBorrower > (allowedSaving * 15)) && (kind == "long")){
                    System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    System.out.println("\t\t\t\t\t\t\t |        Warnings : Please the money you allowed to credit as short term credit                  |");
                    System.out.println("\t\t\t\t\t\t\t |                   should be less than or equal to "+(allowedSaving*15)+"                                      |");;
                    System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                    longTermCredit();
                }
            }
        }
    }

    public static void dontHaveAnotherLoan(String accountId)throws Exception{
        File loanFile = new File("Loan.txt");
        BufferedReader reader = new BufferedReader(new FileReader(loanFile));
        String records;
        while((records = reader.readLine()) != null ){
            if((records.contains(accountId)) && (records.contains("not finished"))){
                System.out.println("\n\n\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                System.out.println("\t\t\t\t\t\t\t |        Warnings : You are not allowed to borrow some money because                                |");
                System.out.println("\t\t\t\t\t\t\t |                   You still have the loan which is not paid                                       |");;
                System.out.println("\t\t\t\t\t\t\t |---------------------------------------------------------------------------------------------------|");
                BankSystemOptions.loanManagement();
            }
            else{

            }
        }
    }
    public static void shortTermLoan()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        Double amountLoan;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO GET SHORT TERM LOAN               |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        validateAuth(accountId,password);
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|      HOW MUCH DO YOU WANT     :              |");
        amountLoan = scan.nextDouble();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateInSaving(accountId,amountLoan,"short");
        dontHaveAnotherLoan(accountId);
        Date today = new Date();
        int account = Integer.parseInt(accountId);
        Loan shortLoan = new Loan(account,amountLoan,5.2,amountLoan,null,"short",today,null);
        shortLoan.saveLoan();
    }

    public static void intermediateLoan()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        Double amountLoan;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO GET INTERMEDIATE TERM LOAN        |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        validateAuth(accountId,password);
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|      HOW MUCH DO YOU WANT     :              |");
        amountLoan = scan.nextDouble();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateInSaving(accountId,amountLoan,"intermediate");
        dontHaveAnotherLoan(accountId);

        Date today = new Date();
        int account = Integer.parseInt(accountId);
        Loan intermediateLoan = new Loan(account,amountLoan,3.2,amountLoan,null,"short",today,null);
        intermediateLoan.saveLoan();
    }

    public static void longTermCredit()throws Exception{
        Scanner scan = new Scanner(System.in);
        String accountId,password;
        Double amountLoan;

        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|             DESTINY BANK  --  LOGIN TO GET LONG TERM LOAN                |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID           :    ");
        accountId = scan.next();
        System.out.print  ("\t\t\t\t|    ENTER PASSWORD             :    ");
        password = scan.next();
        System.out.println("\t\t\t\t|                                                                          |");
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");

        validateAuth(accountId,password);
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        System.out.println("\t\t\t\t|      HOW MUCH DO YOU WANT     :              |");
        amountLoan = scan.nextDouble();
        System.out.println("\t\t\t\t|--------------------------------------------------------------------------|");
        validateInSaving(accountId,amountLoan,"long");
        dontHaveAnotherLoan(accountId);

        Date today = new Date();
        int account = Integer.parseInt(accountId);
        Loan longLoan = new Loan(account,amountLoan,6.2,amountLoan,null,"short",today,null);
        longLoan.saveLoan();
    }
}
