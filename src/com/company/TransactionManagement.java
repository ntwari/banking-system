package com.company;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.StringTokenizer;

public class TransactionManagement {
    public static void saveTransaction(String message)throws Exception{
                /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,message,agentId);
        transaction.saveTransaction();
    }
    public static void allTransaction()throws Exception {
        Transaction.getAllTransaction();
       /*
        SAVING THE TRANSACTION
        */
        File activeAgent = new File("ActiveAgent.txt");
        BufferedReader read= new BufferedReader(new FileReader(activeAgent));
        String agentRecord;
        int agentId = 0;
        while((agentRecord = read.readLine()) != null ){
            StringTokenizer token = new StringTokenizer(agentRecord,",");
            agentId = Integer.parseInt(token.nextToken());
        }
        Transaction transaction = new Transaction(null,null,0,0,"checked all transaction",agentId);
        transaction.saveTransaction();;

        BankSystemOptions.transactionManagement();
    }
    public static void getTransactionByDate()throws Exception{
        String date;
        Scanner scan = new Scanner(System.in);
        System.out.println("\t\t\t\t|-------------------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t|    ENTER DATE TO SEARCH (ex: Fri Apr 24 22:10:20 CAT 2020) :      ");
        date = scan.next();
        System.out.println("\t\t\t\t|-------------------------------------------------------------------------------------|");
        Transaction.searchingTransaction(date);
        saveTransaction("searching transaction with date");
        BankSystemOptions.transactionManagement();
    }

    public static void getTransactionByAgentId()throws Exception {
        String agentId;
        Scanner scan = new Scanner(System.in);
        System.out.println("\t\t\t\t|-------------------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t|    ENTER AGENT ID                      :      ");
        agentId = scan.next();
        System.out.println("\t\t\t\t|-------------------------------------------------------------------------------------|");
        Transaction.searchingTransaction(agentId);
        saveTransaction("searching transaction with agent Id");
        BankSystemOptions.transactionManagement();
    }

    public static void getTransactionByAccountId()throws Exception {
        String agentId;
        Scanner scan = new Scanner(System.in);
        System.out.println("\t\t\t\t|-------------------------------------------------------------------------------------|");
        System.out.print  ("\t\t\t\t|    ENTER ACCOUNT ID                    :      ");
        agentId = scan.next();
        System.out.println("\t\t\t\t|-------------------------------------------------------------------------------------|");
        Transaction.searchingTransaction(agentId);
        saveTransaction("searching transaction with account Id");
        BankSystemOptions.transactionManagement();
    }

}
